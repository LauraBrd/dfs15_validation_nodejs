const MongoCLient = require('mongodb').MongoClient;
const url = "mongodb://localhost:27017/annuaire";
const dbName = "annuaire";

class Mongo {
    constructor() {
        if (!Mongo.instance) {
            MongoCLient.connect(url,
                { userNewUrlParser: true, useUnifiedTopology: true },
                (err, client) => {
                    if (err) throw err;
                    Mongo.instance = client.db(dbName);
                    console.log('mongo connected');
                });
        }
    }
    getInstance() {
        return Mongo.instance;
    }
}

module.exports = new Mongo();
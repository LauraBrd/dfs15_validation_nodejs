var express = require('express');
var router = express.Router();

const ObjectId = require('mongodb').ObjectId;
const mongo = require('../bin/mongo')
const twig = require('twig').twig;



/**
 * List contact order by asc 
 */
router.get('/', function (req, res, next) {
    mongo.getInstance().collection('contacts').find({}, {
        "sort": ['nom', 'asc']
    }).toArray((err, result) => {
        if (err) throw err;
        res.render('listContact', { title: 'Annuaire', result: result });
    })
});


/**
 * Create contact
 */
router.post('/', (req, res) => {
    mongo.getInstance().collection('contacts').find({ email: req.body.email }).toArray(function (err, result) {
        if (err) throw err;
        if (result.length > 0) {
            res.send({ ok: false, message: 'user already exists' })
        } else {
            // Création du contact
            const newContact = {
                avatar: "",
                nom: req.body.nom,
                prenom: req.body.prenom,
                description: req.body.description,
                tel: {
                    tel: req.body.tel
                },
                email: {
                    email: req.body.email
                },
                dateCreate: new Date()
            }
            mongo.getInstance().collection('contacts').insertOne(newContact, (function (err, result) {
                if (err) throw err;
                res.send({ ok: true, message: "Ajout dun nouveau contact effectué." })
            }))
        }
    })
})


/**
 * Update contact
 */
router.put('/:id', (req, res) => {
    mongo.getInstance().collection('contacts').findOne(
        { _id: ObjectId(req.params.id) },
        (err, result) => {
            if (err) throw err;
            if (result) {
                mongo.getInstance().collection('contacts').updateOne(
                    { _id: ObjectId(req.param.id) },
                    { $set: req.body },
                    (err, result) => {
                        if (err) throw err;
                        res.send({ ok: true, result: result });
                    }
                )
            }
        }
    )
})


/**
* Detail contact
*/
router.get('/:id', (req, res) => {
    mongo.getInstance().collection('contacts').findOne(
        { _id: ObjectId(req.params.id) },
        (err, contact) => {
            if (err) throw err;
            res.render('detailContact', { title: 'Détail contact', contact });
        }
    );
});


/**
 * Delete contact
 */
router.delete('/:id', (req, res) => {
    mongo.getInstance().collection('contacts').remove(
        { _id: ObjectId(req.params.id) },
        (err, result) => {
            if (err) throw err;
            res.send({ ok: true });
        }
    )
})



module.exports = router;

var express = require('express');
var router = express.Router();
const mongo = require('../bin/mongo');
var otherRouter = require('./contacts');

/* GET home page. */
router.get('/', function (req, res, next) {
  const nbContact = mongo.getInstance().collection('contacts').find().toArray((err, result) => {
    if (err) throw err;
    const countResult = mongo.getInstance().collection('contacts').count(nbContact);
    const lastContact = mongo.getInstance().collection('contacts').find({}, {
      "sort": ['dateCreate', 'desc']
    }).toArray();
    if (countResult !== null && lastContact !== null) {
      res.render('index', { title: 'Accueil', countResult, lastContact });
      //res.send({ ok: true, countResult: countResult, lastContact: lastContact })

    } else {
      res.send({ ok: false })
    }
  })
});


/**
 * Formulaire ajout contact
 */
router.get('/addContact', function (req, res, next) {
  res.render('addContact', { title: 'Nouveau contact' });
});


module.exports = router;

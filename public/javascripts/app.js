let template = "";
fetch('/template/index').then(result => result.text()).then(result => {
    template = result;
});



const add = () => {
    document.getElementById('formajout').addEventListener('submit', event => {
        event.preventDefault();
        event.stopPropagation();
        const search = event.target.querySelector('input[type=text]');
        fetch('/contacts?formajout=' + search.value).then(result => result.json()).then(result => {
            if (result.ok && result.nbResults) {
                document.getElementById('container').innerHTML = '';
                result.result.forEach(l => {
                    document.getElementById('container').innerHTML += l;
                })
            }
            search.value = "";
        })
    })
};

window.addEventListener('DOMContentLoaded', add);